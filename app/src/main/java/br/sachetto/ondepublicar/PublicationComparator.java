package br.sachetto.ondepublicar;

import java.util.Comparator;

public class PublicationComparator implements Comparator<Publication> {

	@Override
	public int compare(Publication arg0, Publication arg1) {

		if (arg0.classification != null && arg1.classification != null) {
			int i = arg0.classification.compareTo(arg1.classification);
			if (i != 0) return i;

			i = arg0.title.toLowerCase().compareTo(arg1.title.toLowerCase());
			if (i != 0) return i;

			return i;
		}

		return 0;
	}
}
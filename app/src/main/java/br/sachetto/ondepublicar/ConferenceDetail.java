package br.sachetto.ondepublicar;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ConferenceDetail extends AppCompatActivity implements EditConferenceDialog.EditConferenceDialogListener {

    @BindView(R.id.textViewConferenceName) TextView textViewConferenceName;
    @BindView(R.id.textViewAbsDeadline) TextView textViewAbsDeadline;
    @BindView(R.id.textViewPaperDeadline) TextView textViewPaperDeadline;
    @BindView(R.id.textViewWhere) TextView textViewWhere;
    @BindView(R.id.textViewFromDate) TextView textViewFromDate;
    @BindView(R.id.textViewToDate) TextView textViewToDate;
    @BindView(R.id.textViewConferenceUrl) TextView textViewConferenceUrl;

    Conference conf;
    Bundle args;
    WhereToPublish app;
    int confIndex;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.conference_detail);

        app = (WhereToPublish)getApplication();

        ButterKnife.bind(this);

        args = getIntent().getExtras();

        if(args != null) {
            conf = (Conference)args.getSerializable("DATA");
            confIndex = args.getInt("INDEX");

        } else if(savedInstanceState != null) {
            conf = (Conference)savedInstanceState.getSerializable("DATA");
            confIndex = savedInstanceState.getInt("INDEX");
        }

        if(conf == null ) {
            this.finish();
            return;
        }

        textViewConferenceName.setText(conf.title);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(conf.acro + " - " + conf.classification);
        setSupportActionBar(toolbar);
        showInfo();
    }

    private void showInfo() {
        if(conf.when_start != null) {
            textViewFromDate.setText(DateFormat.getDateInstance().format(conf.when_start));
        }

        if(conf.when_end != null) {
            textViewToDate.setText(DateFormat.getDateInstance().format(conf.when_end));
        }

        Date paperDue = conf.paperDeadline;

        if( paperDue != null) {

            textViewPaperDeadline.setText(DateFormat.getDateInstance().format(paperDue));
            if(paperDue.before(new Date())) {
                textViewPaperDeadline.setTextColor(Color.RED);
            }
            else {
                textViewPaperDeadline.setTextColor(Color.BLUE);
            }
        }
        else {
            textViewPaperDeadline.setText(R.string.no_paper_deadline);
        }

        paperDue = conf.abstractDeadline;
        if( paperDue != null) {
            textViewAbsDeadline.setText(DateFormat.getDateInstance().format(paperDue));
            if(paperDue.before(new Date())) {
                textViewAbsDeadline.setTextColor(Color.RED);
            }
            else {
                textViewAbsDeadline.setTextColor(Color.BLUE);
            }
        }
        else {
            textViewAbsDeadline.setText(getResources().getString(R.string.no_abs_deadline));
        }

        if(conf.where != null) {
            textViewWhere.setText(conf.where);

        }

        if(conf.url != null) {
            textViewConferenceUrl.setText(conf.url);
        }
    }

    @Override
    public void onDialogPositiveClick(Conference c) {
        conf = c;

        saveConfData();

        showInfo();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.send_title);
        builder.setMessage(R.string.send_info);

        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                new sendConferenceData().execute("http://getcfps.appspot.com/rest/Conference");
                dialog.dismiss();
            }
        });

        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();

    }

    public void saveConfData() {
        this.app.getPublicationsArray("conferences").set(confIndex, conf);
        app.savePublicationArray("conferences", this);
    }

    private class sendConferenceData extends AsyncTask<String, Void, Void> {


        @Override
        protected Void doInBackground(String... params) {

            JSONHelper.sendJSON(params[0], "{ \"Conference\":" + conf.toJson() + "}");
            return null;
        }
    }


    private class parseSite extends AsyncTask<String, Void, ArrayList<Conference>> {

        protected ArrayList<Conference> doInBackground(String... arg) {
            return searchConferenceInfo(arg[0]);
        }

        protected void onPostExecute(ArrayList<Conference> output) {

            if(output == null || output.size() == 0) {
                AndroidUtils.showToast(getApplicationContext(), getResources().getText(R.string.info_not_found), Toast.LENGTH_LONG);
                return;
            }
            else {
                //The classification is not fetched from the server
                String classification = conf.classification;
                conf = output.get(output.size()-1);
                conf.classification = classification;
                saveConfData();
                showInfo();

            }

            AndroidUtils.showToast(getApplicationContext(), getResources().getString(R.string.info_updated), Toast.LENGTH_LONG);

        }
    }

    private ArrayList<Conference> searchConferenceInfo(String url) {
        try {
            ArrayList<Conference> conferenceInfo = null;
            if(url != null) {
                conferenceInfo = JSONHelper.getConferenceDetails(url);
            }
            return conferenceInfo;

        }
        catch(Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public void getConferenceInfo() {
        if(AndroidUtils.isOnline(getApplicationContext())) {
            AndroidUtils.showToast(getApplicationContext(), getResources().getString(R.string.searching_conf), Toast.LENGTH_LONG);

            String search_url = "";
            try {
                search_url = "http://getcfps.appspot.com/rest/Conference?feq_acro="+
                        URLEncoder.encode(conf.acro, "UTF-8");
                new parseSite().execute(search_url);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }
        else {
            AndroidUtils.showToast(getApplicationContext(), getResources().getString(R.string.device_offline), Toast.LENGTH_LONG);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_conference, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search_info) {
            getConferenceInfo();
            return true;
        }
        else if (id == R.id.action_edit_info) {
            EditConferenceDialog dialog = EditConferenceDialog.newInstance(conf);
            //Se o usuario clicar OK o metodo onDialogPositiveClick é chamado e faz a logica de exportar
            dialog.show(getSupportFragmentManager(), "showEditDialog");
        }
        else if(id == R.id.action_add_calendar) {
            addOnCalendar();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();

        Intent resultIntent = new Intent();
        resultIntent.putExtra("DATA", conf);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

//	@Override
//	public void onClick(View v) {
//		CheckBox b = (CheckBox)v;
//		boolean arg = b.isChecked();
//		CharSequence text;
//		if(arg == true) {
//			text = conf.acro + " " + getResources().getString(R.string.favorite);
//			AndroidUtils.showToast(getApplicationContext(), text,  Toast.LENGTH_SHORT);
//			//ConferenceActivity.FAV_CHANGED = true;
//		}
//		else {
//			text = conf.acro + " " + getResources().getString(R.string.unfavorite);
//			AndroidUtils.showToast(getApplicationContext(), text, Toast.LENGTH_SHORT);
//			//ConferenceActivity.FAV_CHANGED = true;
//		}
//		conf.favorite = arg;
//		ConferenceDetail.ConfChanged = true;
//	}

    public void addOnCalendar() {
        final Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra("allDay", true);

        final boolean[] addDate = new boolean[2];


        if(conf.abstractDeadline != null && conf.paperDeadline != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle(R.string.put_calendar);
            builder.setMessage(R.string.put_calendar_dialog);

            builder.setPositiveButton(R.string.abs_deadline, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    // Do nothing but close the dialog
                    intent.putExtra("beginTime", conf.abstractDeadline.getTime());
                    intent.putExtra("title", getResources().getString(R.string.abs_deadline)+ ": "  + conf.acro);
                    startActivity(intent);
                    dialog.dismiss();
                }
            });

            builder.setNegativeButton(R.string.paper_deadline, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    intent.putExtra("beginTime", conf.paperDeadline.getTime());
                    intent.putExtra("title", getResources().getString(R.string.paper_deadline)+ ": " + conf.acro);
                    startActivity(intent);
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();


        }
        else if (conf.abstractDeadline != null) {
            intent.putExtra("beginTime", conf.abstractDeadline.getTime());
            intent.putExtra("title", getResources().getString(R.string.abs_deadline)+ ": " + conf.acro);
            startActivity(intent);
        }
        else if(conf.paperDeadline != null) {
            intent.putExtra("beginTime", conf.paperDeadline.getTime());
            intent.putExtra("title", getResources().getString(R.string.paper_deadline)+ ": " + conf.acro);
            startActivity(intent);
        }
        else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle(R.string.put_calendar);
            builder.setMessage(R.string.calendar_no_dates);

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();

        }

    }

    	@Override
    	public void onSaveInstanceState(Bundle savedInstanceState) {
    	  super.onSaveInstanceState(savedInstanceState);
    	  savedInstanceState.putSerializable("DATA", conf);
    	}

    	@Override
    	public void onRestoreInstanceState(Bundle savedInstanceState) {
    	  super.onRestoreInstanceState(savedInstanceState);
    	  conf = (Conference)savedInstanceState.getSerializable("DATA");

    	}

}

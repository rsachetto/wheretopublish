package br.sachetto.ondepublicar;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;


public class JournalDetail extends AppCompatActivity {

	@BindView(R.id.textViewConferenceName) TextView textViewConferenceName;
    @BindView(R.id.textViewAbsDeadline) TextView textViewAbsDeadline;
    @BindView(R.id.textViewPaperDeadline) TextView textViewPaperDeadline;
    @BindView(R.id.textViewWhere) TextView textViewWhere;
    @BindView(R.id.textViewFromDate) TextView textViewFromDate;
    @BindView(R.id.textViewToDate) TextView textViewToDate;
    @BindView(R.id.textViewConferenceUrl) TextView textViewConferenceUrl;

	Conference conf;
	Bundle args;
    static boolean ConfChanged;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.conference_detail);

		ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);

		args = getIntent().getExtras();

		if(args != null) {
			conf = (Conference)args.getSerializable("DATA");

		} else if(savedInstanceState != null) {
			conf = (Conference)savedInstanceState.getSerializable("DATA");
		}

		if(conf == null ) {
			this.finish();
			return;
		}

		String name = conf.title;
		String cl = conf.classification;
		String acro = conf.acro;
		String where  = conf.where;
		URL url = null;

		try {
			url = new URL(conf.url);
		}
		catch (MalformedURLException e) {

		}


//		String when = "";
//		if(conf.when_start != null) {
//			when += getResources().getString(R.string.conf_from ) + ": ";
//			when += DateFormat.getDateInstance().format(conf.when_start);
//		}
//
//		String when_to = "";
//		if(conf.when_end != null) {
//			when_to += getResources().getString(R.string.conf_to) + ": ";
//			when_to += DateFormat.getDateInstance().format(conf.when_end);
//		}
//
//		mTextViewWhenFrom.setText(when);
//		mTextViewWhenTo.setText(when_to);
//
//		Date paperDue = conf.paperDeadline;
//
//		if( paperDue != null) {
//			mTextViewDeadlineSub.setText(getResources().getString(R.string.paper_deadline) + " " + DateFormat.getDateInstance().format(paperDue));
//			if(paperDue.before(new Date())) {
//				mTextViewDeadlineSub.setTextColor(Color.RED);
//			}
//			else {
//				mTextViewDeadlineSub.setTextColor(Color.BLUE);
//				calendar_btn.setEnabled(true);
//			}
//		}
//		else {
//			mTextViewDeadlineSub.setText(R.string.no_paper_deadline);
//		}
//
//		paperDue = conf.abstractDeadline;
//		if( paperDue != null) {
//			mTextViewDeadlineAbs.setText(getResources().getString(R.string.abs_deadline) + "  " + DateFormat.getDateInstance().format(paperDue));
//			if(paperDue.before(new Date())) {
//				mTextViewDeadlineAbs.setTextColor(Color.RED);
//			}
//			else {
//				mTextViewDeadlineAbs.setTextColor(Color.BLUE);
//				calendar_btn.setEnabled(true);
//			}
//		}
//		else {
//			mTextViewDeadlineAbs.setText(getResources().getString(R.string.no_abs_deadline));
//		}
	}

	private class parseSite extends AsyncTask<String, Void, ArrayList<String>> {

		protected ArrayList<String> doInBackground(String... arg) {
			return searchConferenceInfo(arg[0]);
		}

		protected void onPostExecute(ArrayList<String> output) {

			if(output == null) {
				AndroidUtils.showToast(getApplicationContext(), getResources().getText(R.string.info_not_found), Toast.LENGTH_LONG);
				return;
			}
			else {
				String abs_deadline = output.get(0);
				String deadline = output.get(1);
				String where = output.get(2);
				String url  = output.get(3);
				Date when_start = null;
				Date when_end = null;

				try {
					when_start = FormatUtils.formatDate(output.get(4),"yyyy-MM-dd'T'HH:mm:ss", Locale.US);
					when_end = FormatUtils.formatDate(output.get(5),"yyyy-MM-dd'T'HH:mm:ss", Locale.US); //2013-06-02T00:00:00;
				} catch (Exception e1) {
					e1.printStackTrace();
				}

				if(deadline != null && !(deadline.equals("Não definido"))) {
					try {
						Date tmp;
						tmp = FormatUtils.formatDate(deadline,"MMM d, yyyy", Locale.US);
						if((conf.paperDeadline == null) || (!tmp.equals(conf.paperDeadline))) {
							conf.paperDeadline = tmp;
							ConfChanged = true;
						}
						//mTextViewDeadlineSub.setText(getResources().getString(R.string.paper_deadline)  + " " + DateFormat.getDateInstance().format(conf.paperDeadline));
                        textViewPaperDeadline.setText(DateFormat.getDateInstance().format(conf.paperDeadline));

						Date paperDue = conf.paperDeadline;
						if(paperDue.before(new Date())) {
                            textViewPaperDeadline.setTextColor(Color.RED);
						}
						else {
                            textViewPaperDeadline.setTextColor(Color.BLUE);
							//calendar_btn.setEnabled(true);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(abs_deadline != null && !(abs_deadline.equals("Não definido"))) {
					try {
						Date tmp;
						tmp = FormatUtils.formatDate(abs_deadline,"MMM d, yyyy", Locale.US);
						if(conf.abstractDeadline == null || !tmp.equals(conf.abstractDeadline)) {
							conf.abstractDeadline = tmp;
							ConfChanged = true;
						}

						//mTextViewDeadlineAbs.setText(getResources().getString(R.string.abs_deadline) + " " + DateFormat.getDateInstance().format(conf.abstractDeadline));
                        textViewAbsDeadline.setText(DateFormat.getDateInstance().format(conf.abstractDeadline));

						Date paperDue = conf.abstractDeadline;
						if(paperDue.before(new Date())) {
                            textViewAbsDeadline.setTextColor(Color.RED);
						}
						else {
                            textViewAbsDeadline.setTextColor(Color.BLUE);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				else {
                    textViewAbsDeadline.setText("-");
				}

				if(where != null) {
					textViewWhere.setText(where);
					conf.where = where;
					ConfChanged = true;
				}

				if(url != null) {

					conf.url = url;
					textViewConferenceUrl.setText(conf.url.toString());

					ConfChanged = true;
				}

				if(when_start != null) {
					conf.when_start = when_start;
                    textViewFromDate.setText(DateFormat.getDateInstance().format(when_start));
				}


				if(when_end != null) {
					conf.when_end = when_end;
                    textViewToDate.setText(DateFormat.getDateInstance().format(when_end));
				}
			}

			AndroidUtils.showToast(getApplicationContext(), getResources().getString(R.string.info_updated), Toast.LENGTH_LONG);

		}
	}

	private ArrayList<String> searchConferenceInfo(String url ) {
		try {
			ArrayList<String> conferenceInfo = null;
			if(url != null) {
				//XMLHelper xh = new XMLHelper(new URL(url));
				//conferenceInfo = xh.getConferenceDetails();
			}
			return conferenceInfo;

		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public void getConferenceInfo(View v) {
		if(AndroidUtils.isOnline(getApplicationContext())) {
			AndroidUtils.showToast(getApplicationContext(), getResources().getString(R.string.searching_conf), Toast.LENGTH_LONG);

			String search_url = "";
			try {
				search_url = "http://getcfps.appspot.com/rest/Conference?feq_acro="+
						URLEncoder.encode(conf.acro, "UTF-8");
				new parseSite().execute(search_url);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

		}
		else {
			AndroidUtils.showToast(getApplicationContext(), getResources().getString(R.string.device_offline), Toast.LENGTH_LONG);
		}
	}

//	@Override
//	public void onClick(View v) {
//		CheckBox b = (CheckBox)v;
//		boolean arg = b.isChecked();
//		CharSequence text;
//		if(arg == true) {
//			text = conf.acro + " " + getResources().getString(R.string.favorite);
//			AndroidUtils.showToast(getApplicationContext(), text,  Toast.LENGTH_SHORT);
//			//ConferenceActivity.FAV_CHANGED = true;
//		}
//		else {
//			text = conf.acro + " " + getResources().getString(R.string.unfavorite);
//			AndroidUtils.showToast(getApplicationContext(), text, Toast.LENGTH_SHORT);
//			//ConferenceActivity.FAV_CHANGED = true;
//		}
//		conf.favorite = arg;
//		ConferenceDetail.ConfChanged = true;
//	}

	public void addOnCalendar(View v) {
		Intent intent = new Intent(Intent.ACTION_EDIT);
		intent.setType("vnd.android.cursor.item/event");
		if(conf.abstractDeadline != null) {
			intent.putExtra("beginTime", conf.abstractDeadline.getTime());
		}
		else {
			intent.putExtra("beginTime", conf.paperDeadline.getTime());
		}
		intent.putExtra("allDay", true);
		intent.putExtra("title", getResources().getString(R.string.conf_calendar_title) + " " + conf.acro);
		startActivity(intent);
	}

//	public boolean onCreateOptionsMenu(Menu menu) {
//		MenuInflater inflater = getMenuInflater();
//		inflater.inflate(R.menu.conf_detail_menu, menu);
//		return true;
//	}
//
//	public boolean onOptionsItemSelected(MenuItem item) {
//		//respond to menu item selection
//		switch (item.getItemId()) {
//		case R.id.send_info:
//			AndroidUtils.sendMail(this, conf);
//			return true;
//
//		default:
//			return super.onOptionsItemSelected(item);
//		}
//	}

	//	@Override
	//	public void onSaveInstanceState(Bundle savedInstanceState) {
	//	  super.onSaveInstanceState(savedInstanceState);
	//	  // Save UI state changes to the savedInstanceState.
	//	  // This bundle will be passed to onCreate if the process is
	//	  // killed and restarted.
	//	  System.out.println("onSaveInstanceState");
	//	  savedInstanceState.putSerializable("conf", conf);	  
	//	  
	//	}
	//	
	//	@Override
	//	public void onRestoreInstanceState(Bundle savedInstanceState) {
	//	  super.onRestoreInstanceState(savedInstanceState);
	//	  // Restore UI state from the savedInstanceState.
	//	  // This bundle has also been passed to onCreate.
	//	  System.out.println("onRestoreInstanceState");
	//	  conf = (Conference)savedInstanceState.getSerializable("conf");
	//	  
	//	}

}

package br.sachetto.ondepublicar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FormatUtils {

    //public static String START_END_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static String START_END_FORMAT = "MMM d, yyyy HH:mm:ss a";
    //public static String DEADLINES_FORMAT = "MMM d, yyyy";
    public static String DEADLINES_FORMAT = "MMM d, yyyy HH:mm:ss a";

	public static Date formatDate(String strDate, String format, Locale locale) throws Exception {
		if (strDate == null || strDate.equals(""))
			return null;
		
        Date date = null;
        try {
        	SimpleDateFormat formatter = new SimpleDateFormat(format, locale);
            date = formatter.parse(strDate);
        } catch (ParseException e) {            
            throw e;
        }
        return date;
	}
	
}

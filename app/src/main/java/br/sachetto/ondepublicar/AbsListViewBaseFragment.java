/*******************************************************************************
 * Copyright 2011-2014 Sergey Tarasevich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package br.sachetto.ondepublicar;

import android.content.Intent;
import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public abstract class AbsListViewBaseFragment extends BaseFragment {

	protected AbsListView listView;
    protected RelativeLayout progressBar = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


	protected void startDetailActivity(String type, Publication publication, int position) {

        Intent intent = new Intent();

        if(type.equalsIgnoreCase("conferences")) {
            intent.setClass(getActivity(), ConferenceDetail.class);
        }
        else {
            intent.setClass(getActivity(), JournalDetail.class);
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable("DATA", publication);
        bundle.putInt("INDEX", position);
        intent.putExtras(bundle);
        startActivity(intent);

	}


    static class PublicationHolder
    {
        TextView txtAcro;
        TextView txtTitle;
        TextView txtClass;
    }

}

package br.sachetto.ondepublicar;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WhereToPublish extends Application {

    public static String CONFERENCES = "conferences";
    public static String JOURNALS = "journals";


    private Map< String, ArrayList<Publication> > publicationsArray;
    int APP_VERSION = 4;


    public WhereToPublish() {
        publicationsArray = new HashMap<>();
        publicationsArray.put(CONFERENCES, new ArrayList<Publication>());
        publicationsArray.put(JOURNALS, new ArrayList<Publication>());
    }

    public void setPublicationsArray(ArrayList<Publication> array, String type) {
        publicationsArray.put(type, array);
    }

    public ArrayList<Publication> getPublicationsArray(String type) {
        return publicationsArray.get(type);
    }

    public String getFileName (String type) {
        return  type + APP_VERSION +".bin";
    }

    public synchronized void configureFirstTime(String type, Activity context) {
        String OLD_FILENAME = type + (APP_VERSION-1) +".bin";
        File old_json;
        old_json = new File(context.getFilesDir().getAbsoluteFile().toString(), OLD_FILENAME);
        if(old_json.exists()) {
            old_json.delete();
        }
        try {
            Resources res = getResources();
            //Abre o json no raw
            int rId;

            if(type.equalsIgnoreCase("conferences")) {
                rId = R.raw.conferences;
            }
            else {
                rId = R.raw.journals;
            }

            InputStream raw = res.openRawResource(rId);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            int i;
            i = raw.read();
            while (i != -1){
                byteArrayOutputStream.write(i);
                i = raw.read();
            }
            raw.close();

            Gson gson = new Gson();
            ArrayList<Publication> publication_array;

            if(type.equalsIgnoreCase(CONFERENCES)) {
                publication_array = gson.fromJson(byteArrayOutputStream.toString(), new TypeToken<ArrayList<Conference>>() {}.getType());
            }
            else {
                publication_array = gson.fromJson(byteArrayOutputStream.toString(), new TypeToken<ArrayList<Journal>>() {}.getType());
            }

            publicationsArray.put(type, publication_array);

            savePublicationArray(type, context);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void savePublicationArray(final String type, final Activity context) {

        Runnable r = new Runnable()
        {
            @Override
            public void run()
            {
                try {
                    Log.d("SAVING", "savePublicationArray");
                    FileOutputStream fos = context.openFileOutput(getFileName(type), Context.MODE_PRIVATE);
                    ObjectOutputStream oo = new ObjectOutputStream(fos);
                    oo.writeObject(publicationsArray.get(type));
                    oo.close();
                    fos.close();
                }
                catch (IOException e) {

                }
            }
        };

        Thread t = new Thread(r);
        t.start();
    }

    @SuppressWarnings("unchecked")
    public synchronized void fillPublicationsArray(String type, Context context) {
        Log.d("FILL", "fillPublicationsArray:" + type);

        if(publicationsArray.get(type) == null || publicationsArray.get(type).size() == 0) {
            FileInputStream fileIn;
            try {
                fileIn = context.openFileInput(getFileName(type));
                ObjectInputStream in = new ObjectInputStream(fileIn);
                publicationsArray.put(type, (ArrayList<Publication>) in.readObject());
                in.close();
                fileIn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}

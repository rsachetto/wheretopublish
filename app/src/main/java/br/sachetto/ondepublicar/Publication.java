package br.sachetto.ondepublicar;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by sachetto on 01/09/16.
 */
public class Publication implements Serializable {

    private static final long serialVersionUID = -6520062588486675127L;

    @Expose String title;
    @Expose String url;

    String classification;
    Boolean favorite;
    boolean dirty;
}

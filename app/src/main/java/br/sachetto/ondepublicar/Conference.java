package br.sachetto.ondepublicar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.text.DateFormat;
import java.util.Date;

public class Conference extends Publication {

	private static final long serialVersionUID = -6520062588486675125L;

	@Expose String acro;
	@Expose String where;
	@Expose Date paperDeadline;
	@Expose Date abstractDeadline;
	@Expose Date when_start;
	@Expose Date when_end;
	
	public Conference() {
		paperDeadline = null;
		abstractDeadline = null;
		favorite = false;
		where = "-";
		url = "-";
        dirty = false;
	}

	public String toJson() {

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(this);
	}

    public String toEmail() {
    	StringBuilder sb = new StringBuilder();
    	sb.append(acro);
    	sb.append("\n");
    	
    	sb.append("Local: ");
    	if(where != null) {
    		sb.append(where);
    	}
		sb.append("\n");
    	
    	sb.append("Deadline submissão: ");
    	if(paperDeadline != null) {
    		sb.append(DateFormat.getDateInstance().format(paperDeadline));
    	}
    	sb.append("\n");
    	
    	sb.append("Deadline abstract: ");
    	if(abstractDeadline != null) {
    		sb.append(DateFormat.getDateInstance().format(abstractDeadline));    		
    	}
    	sb.append("\n");
    	
    	sb.append("Inicio: ");
    	if(when_start != null) {
    		sb.append(DateFormat.getDateInstance().format(when_start));    		
    	}
    	sb.append("\n");
    	
    	sb.append("Fim: ");
    	if(when_end != null) {
    		sb.append(DateFormat.getDateInstance().format(when_end));    		
    	}
    	sb.append("\n");
    	
    	sb.append("Website: ");
    	if(url != null) {
    		sb.append(url);
    	}
    	sb.append("\n");
    	
		return sb.toString();    	
    }
}
 
/*******************************************************************************
 * Copyright 2011-2014 Sergey Tarasevich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package br.sachetto.ondepublicar;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class PublicationListFragment extends AbsListViewBaseFragment {

    //ArrayList<Publication> publication_array;
    private PublicationAdapter adapter = null;
    String type;
    private WhereToPublish app;

    public static PublicationListFragment newInstance(String type) {

        Bundle args = new Bundle();


        args.putString("DATA_TYPE", type);

        PublicationListFragment obj = new PublicationListFragment();
        obj.setArguments(args);

        return obj;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        app = (WhereToPublish)getActivity().getApplication();

        if(args != null) {
            type = args.getString("DATA_TYPE");

        } else if(savedInstanceState != null) {
            type = savedInstanceState.getString("DATA_TYPE");
        }

        populateInfo();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView;

        if(type.equalsIgnoreCase(WhereToPublish.CONFERENCES)) {
            rootView = inflater.inflate(R.layout.fr_conference_list, container, false);
        }
        else {
            rootView = inflater.inflate(R.layout.fr_journal_list, container, false);
        }

        listView = (ListView) rootView.findViewById(android.R.id.list);

        progressBar =  (RelativeLayout) rootView.findViewById(R.id.loadingPanel);

        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startDetailActivity(type, app.getPublicationsArray(type).get(position), position);
            }
        });


        return rootView;
    }


    private void populateInfo() {

        File file;
        file = new File(getActivity().getFilesDir().getAbsoluteFile().toString(), app.getFileName(type));
        if(!file.exists()) {
            new configureFirstTime(getActivity()).execute();
        }
        else {
            new configureListView(getActivity()).execute();
        }
    }

    public void filterData(List<String> options) {
        //TODO: implement filtering
        Log.d("FILTER", "filterData");
    }

    private class configureFirstTime extends AsyncTask<Void, Void, Void> {

        Context context;

        public configureFirstTime(Context context) {
            this.context = context;
        }

        protected Void doInBackground(Void ... unused) {
            app.configureFirstTime(type, getActivity());
            return null;
        }
        protected void onPostExecute(Void unused) {
            //TODO: favoritos
            if(false) {
                getFavoritePublication();
                try {
                    if (progressBar != null) {
                        progressBar.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    // nothing
                }

            }
            else {
                Collections.sort(app.getPublicationsArray(type), new PublicationComparator());
                adapter = new PublicationAdapter(app.getPublicationsArray(type), context, type);
                listView.setAdapter(adapter);
                try {
                    //    progress.dismiss();
                    if (progressBar != null) {
                        progressBar.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    // nothing
                }
            }
        }
    }

    private class configureListView extends AsyncTask<Void, Void, Void> {

        Context context;

        public configureListView(Context context) {
            this.context = context;
        }

        protected Void doInBackground(Void ... unused) {
            app.fillPublicationsArray(type, getActivity());
            return null;
        }
        protected void onPostExecute(Void unused) {
            //TODO Favoritos
            if(false) {
                getFavoritePublication();
                try {
                    if (progressBar != null) {
                        progressBar.setVisibility(View.GONE);
                    }
                } catch (Exception ignored) {

                }
            }
            else {
                Collections.sort(app.getPublicationsArray(type), new PublicationComparator());
                adapter = new PublicationAdapter(app.getPublicationsArray(type), context, type);
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                try {
                    if (progressBar != null) {
                        progressBar.setVisibility(View.GONE);
                    }
                } catch (Exception ignored) {

                }
            }
        }
    }



    void getFavoritePublication() {
/*        fav_conf.clear();
        for (Conference aConf_array : publication_array) {
            if (aConf_array.favorite) {
                fav_conf.add(aConf_array);
            }
        }
        Collections.sort(fav_conf, new br.sachetto.ondepublicar.PublicationComparator());
        this.setListAdapter(new PublicationAdapter(getActivity(),  R.layout.listview_item_row, fav_conf, only_fav));*/
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(type.equalsIgnoreCase(WhereToPublish.CONFERENCES)) {
            super.onCreateOptionsMenu(menu, inflater);

            inflater.inflate(R.menu.menu_main, menu);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_filter) {
            FilterConferenceDialog filterConferenceDialog = new FilterConferenceDialog();
            filterConferenceDialog.show(getActivity().getSupportFragmentManager(), "showFilterDialog");
            return true;
        }



        return super.onOptionsItemSelected(item);
    }

    private static class PublicationAdapter extends BaseAdapter {

        private ArrayList<Publication> _data;

        private Context _c;

        private LayoutInflater inflater;

        private String pubType;

        PublicationAdapter(ArrayList<Publication> data, Context context, String pubType) {
            //CERApplication.initImageLoader(context);
            _data = data;
            _c = context;
            this.pubType = pubType;
            inflater = LayoutInflater.from(context);

        }

        @Override
        public int getCount() {
            return _data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            final PublicationHolder holder;
            final ImageButton btn;

            if (convertView == null) {
                holder = new PublicationHolder();
                if(pubType.equalsIgnoreCase(WhereToPublish.CONFERENCES)) {
                    view = inflater.inflate(R.layout.conference_row, parent, false);
                    holder.txtAcro = (TextView) view.findViewById(R.id.txtAcro);
                }
                else {
                    view = inflater.inflate(R.layout.conference_row, parent, false);
                }

                holder.txtTitle = (TextView) view.findViewById(R.id.txtTitle);
                holder.txtClass = (TextView) view.findViewById(R.id.txtClass);

                view.setTag(holder);

            } else {
                holder = (PublicationHolder) view.getTag();
            }

            Publication publication = _data.get(position);

            holder.txtTitle.setText(publication.title);
            holder.txtClass.setText(publication.classification);

            if(pubType.equalsIgnoreCase(WhereToPublish.CONFERENCES)) {
                final Conference conf = (Conference)publication;
                holder.txtAcro.setText(conf.acro);
                if(view != null) {
                    //take the Button and set listener. It will be invoked when you click the button.
                    btn = (ImageButton) view.findViewById(R.id.buttonFav);
                    btn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            conf.favorite = !conf.favorite;

                            if (conf.favorite) {
                                btn.setImageResource(R.drawable.ic_star_black_24dp);
                            }
                            else {
                                btn.setImageResource(R.drawable.ic_star_border_black_24dp);
                            }

                        }

                    });

                    if (conf.favorite) {
                        btn.setImageResource(R.drawable.ic_star_black_24dp);
                    }
                    else {
                        btn.setImageResource(R.drawable.ic_star_border_black_24dp);
                    }

                }
            }

            return view;
        }

    }

//    private class OnStarClickListener implements View.OnClickListener {
//        private int mPosition;
//        private CheckBox check_box;
//        //private View mRow;
//        OnStarClickListener(int position, CheckBox b, View row){
//            mPosition = position;
//            this.check_box = b;
//            // this.mRow = row;
//        }
//        @Override
//        public void onClick(View arg0) {
////            boolean arg = check_box.isChecked();
////            CharSequence text;
////            Conference conf = data.get(mPosition);
////
////            if(arg == true) {
////                text = conf.acro + " " + context.getResources().getString(R.string.favorite);
////                AndroidUtils.showToast(context, text,  Toast.LENGTH_SHORT);
////                ConferenceActivity.FAV_CHANGED = true;
////            }
////            else {
////                text = conf.acro + " " + context.getResources().getString(R.string.unfavorite);
////                AndroidUtils.showToast(context, text, Toast.LENGTH_SHORT);
////                ConferenceActivity.FAV_CHANGED = true;
////                if(only_fav) {
////                    ConferenceActivity act = (ConferenceActivity) context;
////                    act.updateFavorites(mPosition);
////                    data.remove(mPosition);
////                    //mRow = null;
////                    notifyDataSetChanged();
////                }
////            }
////            conf.favorite = arg;
////            br.sachetto.ondepublicar.ConferenceDetail.ConfChanged = true;
//        }
//    }


}

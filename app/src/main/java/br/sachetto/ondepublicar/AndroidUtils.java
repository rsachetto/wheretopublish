package br.sachetto.ondepublicar;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Date;

public class AndroidUtils {

	static public boolean isOnline(Context c) {
		ConnectivityManager cm =
				(ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}	
		return false;
	}

	static public void showToast(Context c, CharSequence text, int duration) {		
		Toast toast = Toast.makeText(c, text, duration);
		toast.show();
	}

	static public void sendMail(Context c, Conference conf) {
		try {			
			final Intent emailIntent = new Intent(Intent.ACTION_SEND);

			/* Fill it with Data */
			emailIntent.setType("plain/text");
			emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"sachetto@outlook.com"});
			emailIntent.putExtra(Intent.EXTRA_SUBJECT,  URLEncoder.encode(conf.acro, "UTF-8"));
			emailIntent.putExtra(Intent.EXTRA_TEXT,  conf.toEmail());

			/* Send it off to the Activity-Chooser */
			c.startActivity(Intent.createChooser(emailIntent, c.getResources().getString(R.string.send_mail)));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();

		}
	}

	static public Date getDate(int year, int monthOfYear, int dayOfMonth) {

		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(0);
		cal.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
		Date chosenDate = cal.getTime();

		return  chosenDate;
	}

}

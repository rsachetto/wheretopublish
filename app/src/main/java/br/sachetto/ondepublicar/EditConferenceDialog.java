package br.sachetto.ondepublicar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

public class EditConferenceDialog extends DialogFragment {

    Conference conf;
    Date newAbsDate, newPaperDate, newFromDate, newToDate;

    /* The activity that creates an instance of this dialog fragment must
    * implement this interface in order to receive event callbacks.
    * Each method passes the DialogFragment in case the host needs to query it. */
    public interface EditConferenceDialogListener {
        public void onDialogPositiveClick(Conference conf);
    }

    // Use this instance of the interface to deliver action events
    EditConferenceDialogListener mListener;

    public static EditConferenceDialog newInstance(Conference conf) {
        EditConferenceDialog f = new EditConferenceDialog();
//
//        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putSerializable("DATA", conf);
        f.setArguments(args);

        return f;
    }

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (EditConferenceDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflator = getActivity().getLayoutInflater();

        Bundle args = getArguments();

        if(args != null) {
            conf = (Conference)args.getSerializable("DATA");
        }

        newAbsDate = conf.abstractDeadline;
        newPaperDate = conf.paperDeadline;
        newFromDate  = conf.when_start;
        newToDate = conf.when_end;

        View dialogView = inflator.inflate(R.layout.dialog_edit_conference, null);


        final EditText editTextAbsDeadline = (EditText) dialogView.findViewById(R.id.editTextAbsDeadline);
        final EditText editTextPaperDeadline = (EditText) dialogView.findViewById(R.id.editTextPaperDeadline);
        final EditText editTextDateFrom = (EditText) dialogView.findViewById(R.id.editTextDateFrom);
        final EditText editTextDateTo = (EditText) dialogView.findViewById(R.id.editTextDateTo);
        final EditText editTextWhere = (EditText) dialogView.findViewById(R.id.editTextWhere);
        final EditText editTextURL = (EditText) dialogView.findViewById(R.id.editTextURL);

        editTextAbsDeadline.setKeyListener(null);
        editTextPaperDeadline.setKeyListener(null);
        editTextDateFrom.setKeyListener(null);
        editTextDateTo.setKeyListener(null);

        if(conf.abstractDeadline != null) {
            editTextAbsDeadline.setText(DateFormat.getDateInstance().format(conf.abstractDeadline));
        }

        if(conf.paperDeadline != null) {
            editTextPaperDeadline.setText(DateFormat.getDateInstance().format(conf.paperDeadline));
        }

        if(conf.when_start != null) {
            editTextDateFrom.setText(DateFormat.getDateInstance().format(conf.when_start));
        }

        if(conf.when_end != null) {
            editTextDateTo.setText(DateFormat.getDateInstance().format(conf.when_end));
        }

        if(conf.where != null) {
            editTextWhere.setText(conf.where);
        }

        if(conf.url != null) {
            editTextURL.setText(conf.url);
        }

        final ImageButton buttonAbsDeadline = (ImageButton) dialogView.findViewById(R.id.buttonAbsDeadline);
        final ImageButton buttonAbsDeadlineClear = (ImageButton) dialogView.findViewById(R.id.buttonAbsDeadlineClear);

        final ImageButton buttonPaperDeadline = (ImageButton) dialogView.findViewById(R.id.buttonPaperDeadline);
        final ImageButton buttonPaperDeadlineClear = (ImageButton) dialogView.findViewById(R.id.buttonPaperDeadlineClear);

        final ImageButton buttonDateFrom = (ImageButton) dialogView.findViewById(R.id.buttonDateFrom);
        final ImageButton buttonDateFromClear = (ImageButton) dialogView.findViewById(R.id.buttonDateFromClear);

        final ImageButton buttonDateTo = (ImageButton) dialogView.findViewById(R.id.buttonDateTo);
        final ImageButton buttonDateToClear = (ImageButton) dialogView.findViewById(R.id.buttonDateToClear);


        buttonAbsDeadline.setOnClickListener(new View.OnClickListener() {
                                                 @Override
                                                 public void onClick(View v) {
                                                     //Calendar now = Calendar.getInstance();
                                                     final Calendar c = Calendar.getInstance();

                                                     DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                                                             new DatePickerDialog.OnDateSetListener() {

                                                                 @Override
                                                                 public void onDateSet(DatePicker view, int year,
                                                                                       int monthOfYear, int dayOfMonth) {
                                                                     Date tmp = AndroidUtils.getDate(year, monthOfYear, dayOfMonth);
                                                                     editTextAbsDeadline.setText(DateFormat.getDateInstance().format(tmp));
                                                                     newAbsDate = tmp;

                                                                 }
                                                             }, c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DATE));
                                                     dpd.show();
                                                 }
                                             }

        );


        buttonAbsDeadlineClear.setOnClickListener(new View.OnClickListener() {
                                                      @Override
                                                      public void onClick(View v) {
                                                          editTextAbsDeadline.setText("");
                                                          newAbsDate = null;
                                                      }
                                                  }

        );

        buttonPaperDeadline.setOnClickListener(new View.OnClickListener() {
                                                   @Override
                                                   public void onClick(View v) {
                                                       //Calendar now = Calendar.getInstance();
                                                       final Calendar c = Calendar.getInstance();

                                                       DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                                                               new DatePickerDialog.OnDateSetListener() {

                                                                   @Override
                                                                   public void onDateSet(DatePicker view, int year,
                                                                                         int monthOfYear, int dayOfMonth) {

                                                                       Date tmp = AndroidUtils.getDate(year, monthOfYear, dayOfMonth);
                                                                       editTextPaperDeadline.setText(DateFormat.getDateInstance().format(tmp));
                                                                       newPaperDate = tmp;

                                                                   }
                                                               }, c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DATE));
                                                       dpd.show();
                                                   }
                                               }

        );

        buttonPaperDeadlineClear.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            editTextPaperDeadline.setText("");
                                                            newPaperDate = null;
                                                        }
                                                    }

        );

        buttonDateFrom.setOnClickListener(new View.OnClickListener() {
                                              @Override
                                              public void onClick(View v) {
                                                  //Calendar now = Calendar.getInstance();
                                                  final Calendar c = Calendar.getInstance();

                                                  DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                                                          new DatePickerDialog.OnDateSetListener() {

                                                              @Override
                                                              public void onDateSet(DatePicker view, int year,
                                                                                    int monthOfYear, int dayOfMonth) {

                                                                  Date tmp = AndroidUtils.getDate(year, monthOfYear, dayOfMonth);
                                                                  editTextDateFrom.setText(DateFormat.getDateInstance().format(tmp));
                                                                  newFromDate = tmp;

                                                              }
                                                          }, c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DATE));
                                                  dpd.show();
                                              }
                                          }

        );

        buttonDateFromClear.setOnClickListener(new View.OnClickListener() {
                                                   @Override
                                                   public void onClick(View v) {
                                                       editTextDateFrom.setText("");
                                                       newFromDate = null;
                                                   }
                                               }

        );


        buttonDateTo.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                //Calendar now = Calendar.getInstance();
                                                final Calendar c = Calendar.getInstance();

                                                DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                                                        new DatePickerDialog.OnDateSetListener() {

                                                            @Override
                                                            public void onDateSet(DatePicker view, int year,
                                                                                  int monthOfYear, int dayOfMonth) {

                                                                Date tmp = AndroidUtils.getDate(year, monthOfYear, dayOfMonth);
                                                                editTextDateTo.setText(DateFormat.getDateInstance().format(tmp));
                                                                newToDate = tmp;

                                                            }
                                                        }, c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DATE));
                                                dpd.show();
                                            }
                                        }

        );

        buttonDateToClear.setOnClickListener(new View.OnClickListener() {
                                                 @Override
                                                 public void onClick(View v) {
                                                     editTextDateTo.setText("");
                                                     newToDate = null;
                                                 }
                                             }

        );

        // Set the dialog title
        builder.setTitle(getString(R.string.edit_info))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        try {

                            conf.abstractDeadline = newAbsDate;
                            conf.paperDeadline = newPaperDate;
                            conf.when_start = newFromDate;
                            conf.when_end = newToDate;

                            conf.where = editTextWhere.getText().toString();
                            conf.url = editTextURL.getText().toString();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        mListener.onDialogPositiveClick(conf);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                })
                .setView(dialogView);

        return builder.create();
    }

}

package br.sachetto.ondepublicar;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.impl.client.CloseableHttpClient;
import cz.msebera.android.httpclient.impl.client.HttpClients;
import cz.msebera.android.httpclient.util.EntityUtils;

public class JSONHelper {

	public static void sendJSON(String url, String json) {
		try {
			// defaultHttpClient
			CloseableHttpClient httpClient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(url);
			httpPost.addHeader("Content-Type", "application/json");
			StringEntity data = new StringEntity(json);

			httpPost.setEntity(data);
			httpClient.execute(httpPost);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String getJSONFromUrl(String url) {
		String json = null;


		try {
			// defaultHttpClient
			CloseableHttpClient httpClient = HttpClients.createDefault();
			HttpGet httpGet = new HttpGet(url);
			httpGet.addHeader("Accept","application/json");

            CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
            try {
                HttpEntity httpEntity = httpResponse.getEntity();
                json = EntityUtils.toString(httpEntity, "UTF-8");

            } finally {
                httpResponse.close();
            }

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();
		}
		// return JSON
		return json;
	}

	public static ArrayList<Conference> getConferenceDetails(String url) {
        String json = getJSONFromUrl(url);

        if(json.contains("Conference")) {

            if (json.contains("[")) {
                json = json.replace("{\"list\": {\"Conference\":", "");
                json = json.replace(", \"@offset\": \"\"}}", "");
            } else {
                json = json.replace("{\"list\": {\"Conference\":", "[");
                json = json.replace(", \"@offset\": \"\"}}", "]");
            }


            return new Gson().fromJson(json, new TypeToken<ArrayList<Conference>>() {
            }.getType());
        }
        else {
            return null;
        }
	} 


}
